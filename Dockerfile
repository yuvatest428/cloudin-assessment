# Use a base image with Python installed
FROM python:3

# working directory
WORKDIR /app

# Copy our code to the working directory
COPY app.py .

# execution
CMD ["python", "app.py"]
